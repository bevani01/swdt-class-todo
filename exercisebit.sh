#!/bin/bash

rm -rf "swdt-lass-todo"
git clone bevani01@bitbucket.org:bevani01/swdt-class-todo.git
cd swdt-class-todo
find . -name "*.pyc" -exec rm {} +
find . -name "__pycache__" -exec rm -rf {} +
find . -name "*.class" -exec rm {} +
find . -name "*.out" -exec rm {} +
find . -name "*.txt" -exec rm {} +
git add .
git commit -m "cleaned up files"
git push origin master
cd ..
cp 'exercisebit.sh' swdt-class-todo
cd swdt-class-todo
git add 'exercisebit.sh'
git commit -m 'Run to clean Repository'
git push

